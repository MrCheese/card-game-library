This is a library containing Classes useful in card games.  It was written in C# using .NET 4.5 and was my starting point for building an Online version of Cryptozoic's [Street Fighter Deck-Building Game](https://www.cryptozoic.com/games/capcom-street-fighter-deck-building-game).

There are four classes included in this library. One of the included classes includes a Deck class which implements the ICollection<T> interface of [System.Collections.Generic](https://msdn.microsoft.com/en-us/library/92t2ye13(v=vs.110).aspx) and includes Methods for shuffling the deck, drawing a card and adding cards to a deck.  There are also three base classes for Cards, Players and CardGame ( a class to keep track of the game state ). 

To use this library either 

* compile the project using .NET 4.5 or later and add the DLL as a reference to your project.
* add the project to an existing solution and add the a reference to this project into the references in the previously existing solution.
* download the sample project found at [here](https://bitbucket.org/MrCheese/card-game-library-sample). The sample project creates a deck of 5 cards with int values 1-5 and allows for shuffling and drawing cards.

Unfortunately due to the proprietary content of [Street Fighter Deck-Building Game](https://www.cryptozoic.com/games/capcom-street-fighter-deck-building-game), I am unable to post that project.


Andrew Cheesman