﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGameLibrary
{
    /* CardGame is an abstract class whose implementation should keep track of a gamestate.
     * It includes properties commonly used in Card Games, like the current player's turn and Collections of cards. */
    public abstract class CardGame
    {
        protected int _activePlayer = 0; // The player who's turn it currently is
        protected List<Player> _players = new List<Player>(); // List of players in game
        protected Dictionary<String, ICollection<Card>> _card_locations = new Dictionary<string,ICollection<Card>>();  // Locations which cards can exist, decks, player hands, discard piles

        public abstract void PlayCard(Card card);
        public abstract void EndTurn();
    }
}
