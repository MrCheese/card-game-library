﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGameLibrary
{
    /* A player class keeping track of a player. Depending on the card game being implemented, 
     * this class may store things such as player hands/decks/discard piles/points, etc. */
    public class Player
    {
        String _id;
        public String Id
        { get { return _id; } }

        public Player(String id)
        {
            _id = id;
        }
    }
}
