﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGameLibrary
{
    // The Deck class implements ICollection and provides additionaly functionality such as shuffling the deck.
    public class Deck<T> : ICollection<T> where T : Card
    {
        protected List<T> _cards; //Contents of collection.  The top of the deck will be the last ellement in the List and the Top will be the first
        protected bool _isReadOnly;
        private Random _ran; //Declared globally to maintain statistical requirements which which may be hurt if declared in function.
        

        public Deck()
        {
            _cards = new List<T>();
            _isReadOnly = false;
            _ran = new Random();
        }

        // Default accessor for the collection 
        public virtual T this[int index]
        {
            get { return _cards[index];  }
            set { _cards[index] = value; }
        }

        #region Properties and Methods required by ICollection
        //Properties
        // Number of elements in the collection 
        public virtual int Count
        {
            get { return _cards.Count; }
        }

        // Is the deck readonly? 
        public virtual bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        //Methods
        // Adds a card to the Deck<T>.
        public virtual void Add(T card)
        {
            _cards.Add(card);
        }

        // Removes all cards from the Deck<T>.
        public void Clear()
        {
            _cards.Clear();
        }

        // Determines whether the Deck<T> contains a specific value.
        public bool Contains(T card)
        {
            return _cards.Contains(card);
        }

        // Copies the cards of the Deck<T> to an Array, starting at a particular Array index.
        public void CopyTo(T[] array, int arrayIndex)
        {
            _cards.CopyTo(array, arrayIndex);
        }

        // Returns an enumerator that iterates through the deck. (Inherited from IEnumerable<T>.)
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _cards.GetEnumerator();
        }

        // Remove the first occurrence (bottom to top) of the card from Deck<T>.
        public virtual bool Remove(T card)
        {
            return _cards.Remove(card);
        }
        #endregion

        // Add a card to the collection at the top of the Deck
        public virtual void AddToTop(T card)
        {
            this.Add(card);
        }

        // Add a card to the collection at the bottom of the Deck
        public virtual void AddToBotom(T card)
        {
            _cards.Insert(0, card);
        }

        // Insert card into specified position in the Deck
        public virtual void Insert(int position, T card)
        {
            _cards.Insert(position, card);
        }

        /* Removes the top card from the deck and returns it. 
         * Returns null if the deck is empty. */
        public virtual T Draw()
        {
            if (_cards.Count == 0) return null; // No card to draw, return null.

            int index = _cards.Count - 1; // Index of the card we are drawing.
            T topCard = _cards[index]; // Top card is last card in List<T> _cards.
            _cards.RemoveAt(index); 

            return topCard;
        }

        /* Shuffles the deck of cards. 
         * Shuffling is performed by an abstract devision of the deck into two paarts;
         * an unshuffled part and a shuffled part. Cards from the unshuffled part are selected
         * at random and moved to the shuffled part. */
        public virtual void Shuffle()
        {
            int numUnShuffled = _cards.Count;  // The number of cards not yet shuffled.
            for (int i = 1; i < _cards.Count; ++i) // Start i at 1, since we only need to swap N-1 times
                SwapCards(_cards.Count - i, _ran.Next(numUnShuffled--)); // Swap the random card with the unshuffled card bordering the shuffled/unshuffled regions
        }

        // Swap the position of two cards in the deck
        public virtual void SwapCards(int i, int j)
        {
            T temp = _cards[i];
            _cards[i] = _cards[j];
            _cards[j] = temp;
        }

        // Returns a custom generic enumerator that iterates through the collection.
        public virtual IEnumerator<T> GetEnumerator()
        {
            return _cards.GetEnumerator();
        }
    }
}
